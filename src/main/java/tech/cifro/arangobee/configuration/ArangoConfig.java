package tech.cifro.arangobee.configuration;

import com.arangodb.ArangoDB;
import com.github.arangobee.Arangobee;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ArangoConfig {

    private final String host;
    private final int port;
    private final String username;
    private final String password;
    private final String database;


    public ArangoConfig(@Value("${arangodb.host}") String host,
                        @Value("${arangodb.port}") int port,
                        @Value("${arangodb.username}") String username,
                        @Value("${arangodb.password}") String password,
                        @Value("${arangodb.database}") String database) {
        this.host = host;
        this.port = port;
        this.username = username;
        this.password = password;
        this.database = database;
    }


    public String database() {
        return database;
    }

    @Bean
    public Arangobee arangobee() {

        ArangoDB arangoDB = new ArangoDB.Builder()
                .host(host, port)
                .user(username)
                .password(password)
                .build();

        Arangobee arangobee = new Arangobee(arangoDB.db(database()), null);

        arangobee.setChangeLogsScanPackage("com.vpavlova.changelogs");

        return arangobee;
    }

}
