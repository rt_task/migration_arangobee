package tech.cifro.arangobee.changelogs;

import com.arangodb.ArangoDBException;
import com.arangodb.ArangoDatabase;
import com.github.arangobee.changeset.ChangeLog;
import com.github.arangobee.changeset.ChangeSet;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Map;

@Slf4j
@ChangeLog
public class DatabaseChangelog {

    private static final String CHANGESETS_PATH = "src/main/resources/migration/";



    @ChangeSet(order = "001", id = "ch_001", author = "devAuthor")
    public void change001(ArangoDatabase db) {

        String aqlName = "update_cities.aql";
        execute(db, readAQL(aqlName), aqlName);
    }

    @ChangeSet(order = "002", id = "ch_002", author = "anotherAuthor")
    public void change002(ArangoDatabase db) {

        String aqlName = "update_cities_2.aql";
        execute(db, readAQL(aqlName), aqlName);
    }




    private String readAQL(String filename) {
        try {
            return new String(Files.readAllBytes(Paths.get(CHANGESETS_PATH, filename)));
        }
        catch (IOException e) {
            log.error("Couldn't read file {}. Error: ", filename, e);
            throw new RuntimeException(e);
        }
    }

    private void execute(ArangoDatabase db, String aql, String changeSet)
    {
        try {
            db.query(aql, Map.class);
        }
        catch (ArangoDBException e) {
            log.error("Ошибка при выполнении changeSet скрипта {}: {}", changeSet, e);
            throw new RuntimeException(e);
        }
    }
}


