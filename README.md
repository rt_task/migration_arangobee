# Миграция скриптов AQL в базу данных ArangoDB



## Настройка параметров подключения к базе данных

1. Откройте файл `application.yml` в корне проекта.
2. Найти раздел, отвечающий за параметры подключения к базе данных.
3. Укажите следующие параметры:

- **host**: Адрес хоста базы данных.

- **port**: Порт для подключения к базе данных.

- **username**: Имя пользователя для аутентификации.

- **password**: Пароль пользователя.

- **database**: Название целевой базы данных.

### Example 
```yml
spring:
  data:
    arangodb:
      host: 10.1.178.25
      port: 32010
      username: root
      password: e0eac24f19642b559fd989bfa84f60856238d36f2fc22e2581d80c57c35946a0
      database: micro-registry-results
```
## Сборка приложения
Выполните команду `mvn clean install`
## Запуск приложения 
Выполните команду `mvn spring-boot:run` для запуска приложения.

#Работа с базой данных ArangoDB
1. Необходимо развернуть базу данных ArangoDB в докере: 
    - запустить файл \src\main\resources\docker-compose.yml с помощью командной строки `docker compose up`
    - далее требуется зайти в браузер ро адресу `http://localhost:8529` логин\пароль root\root
    - создать базу данных для работы (затем ее название указывается в `application.yml`)
   ![img.png](img.png)
2. В созданную базу данных необходимо импортировать коллекцию
   Add Collection -> Name (указать название коллекции) -> Download Documents as JSON File 
   ![img_1.png](img_1.png)
   в папке \src\main\resources\db\russia_cities.json можно использовать json для импорта коллекции.
3. В классе DatabaseChangelog.java реализовано 2 метода @ChangeSet:
   - метод insertInTable, в котором aql описан внутри метода;
   - метод insertInTableFromFile в нем aql используется из папки \src\main\resources\migration\update_cities.aql\
   
**Параметры аннотации @ChangeSet** 
- **order** — строка для сортировки наборов изменений.
Сортировка в алфавитном порядке по возрастанию. Это может быть число, дата и т.д.
- **id** — имя набора изменений, должно быть ***уникальным*** для всех журналов изменений в базе данных.
- **автор** - автор набора изменений
- **runAlways** — [необязательно, по умолчанию: false] набор изменений будет выполняться всегда, но в коллекции dbchangelog будет храниться только событие первого выполнения.
4. Необходимо отобразить в aql изменения для коллекции базы данных и запустить приложение.

> ОБРАТИТЕ ВНИМАНИЕ!\
> ТАК КАК УНИКАЛЬНОСТЬ ПАРАМЕТРОВ АННОТАЦИИ @CHANGESET КРАЙНЕ ВАЖНО ПРИ ОБНОВЛЕНИИ КОЛЛЕКЦИИ. 

## Инструкция для переноса AQL-скриптов в базу данных ArangoDB

1. Добавить в конфигурационный план проекта необходимые параметры
для подключения к БД;
2. Также требуется добавить следующие зависимости в maven/gradle:
```       
        <dependency>
            <groupId>io.github.cmoine</groupId>
            <artifactId>arangobee</artifactId>
            <version>0.17</version>
        </dependency>
        <dependency>
            <groupId>com.arangodb</groupId>
            <artifactId>arangodb-java-driver</artifactId>
            <version>6.2.0</version>
        </dependency>
        <dependency>
            <groupId>org.projectlombok</groupId>
            <artifactId>lombok</artifactId>
            <version>1.18.28</version>
        </dependency>
```
```
dependencies {
    compile 'org.projectlombok:lombok:1.18.28'
    compile 'org.javassist:javassist:3.18.2-GA' 
    compile 'com.github.arangobee:arangobee:0.17'
}
```
3. Добавить в проект классы для миграции **DatabaseChangelog.java и ArangoConfig.java**;
4. В папку migration добавить файл .aql, который содержит необходимые обновления для базы данных;
5. В классе **DatabaseChangelog.java** требуется создать метод с аннотацией @ChangeSet, где aqlName присвоить 
имя файла со скриптом, ориентируясь на примеры методов change001 и change002;



## Разработчик

* **NAME**: Павлова Виктория
* **E-MAIL**: viktoriya.n.pavlova@rt.ru

## Руководитель
* **NAME**: Назаров Кристиан
* **E-MAIL**: kristian.nazarov@rt.ru